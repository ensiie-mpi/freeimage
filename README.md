What is FreeImage ?
-----------------------------------------------------------------------------
FreeImage is an Open Source library project for developers who would like to support popular graphics image formats like PNG, BMP, JPEG, TIFF and others as needed by today's multimedia applications.
FreeImage is easy to use, fast, multithreading safe, and cross-platform (works with Windows, Linux and Mac OS X).

Thanks to it's ANSI C interface, FreeImage is usable in many languages including C, C++, VB, C#, Delphi, Java and also in common scripting languages such as Perl, Python, PHP, TCL, Lua or Ruby.

The library comes in two versions: a binary DLL distribution that can be linked against any WIN32/WIN64 C/C++ compiler and a source distribution.
Workspace files for Microsoft Visual Studio provided, as well as makefiles for Linux, Mac OS X and other systems.

Installation dans votre home de FreeImage pour TD ENSIIE
-----------------------------------------------------------------------------
FreeImage est déjà installé mais si vous souhaitez l'installer dans votre home ou sur votre ordinateur :
```
cd FreeImage/
make -j 8
make install
```

Le produit s'installe dans votre home dans le répertoire `~/softs/FreeImage`.

Pour les exercices des TDs, si vous voulez utiliser votre installation de FreeImage :
```
export FREEIMAGE_ROOT=/votre/home/softs/FreeImage
```

A noter : pour un usage correct dans le cadre de nos TDs, forcer l'utilisation de la lib statique `libfreeimage.a`.

